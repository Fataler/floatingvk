package org.ffcoders.floatingvk;

import wei.mark.standout.ui.Window;
import android.content.res.Configuration;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

public class WidgetsWindow extends org.ffcoders.floatingvk.MultiWindow {
	public static final int DATA_CHANGED_TEXT = 0;

	@Override
	public void createAndAttachView(final int id, FrameLayout frame) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.widgets, frame, true);
		WebView webView =(WebView) view.findViewById(R.id.webView2);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new HWebViewClient());
		webView.loadUrl("http://m.vk.com/");


	}

	@Override
	public StandOutLayoutParams getParams(int id, Window window) {
		Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		if (width>height){
			//horizontal
		return new StandOutLayoutParams(id, (int) (width*.45), (int) (height * .8),
				StandOutLayoutParams.LEFT, StandOutLayoutParams.TOP);}
		else{
			//vertical
			return new StandOutLayoutParams(id, (int) (width*.75) , (int) (height * .8),
					StandOutLayoutParams.LEFT, StandOutLayoutParams.TOP);}

	}

	@Override
	public String getAppName() {
		return "Floating VK";
	}

	@Override
	public int getThemeStyle() {
		return android.R.style.Theme_NoDisplay;
	}

	/*
    public void onConfigurationChanged(Configuration newConfig ) {
		super.onConfigurationChanged(newConfig);

		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            //getFocusedWindow().setLayoutParams(getParams(DEFAULT_ID, getFocusedWindow()));

		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){

			//StandOutWindow.closeAll(this, WidgetsWindow.class);
			//StandOutWindow.show(this, WidgetsWindow.class, StandOutWindow.DEFAULT_ID);

           // getFocusedWindow().setLayoutParams(getParams(DEFAULT_ID,getFocusedWindow()));
		}
	}
*/
}

class HWebViewClient extends WebViewClient
{
	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url)
	{
		view.loadUrl(url);
		return true;
	}
}
